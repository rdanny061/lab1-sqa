import { Cliente } from './cliente.model';

describe('Cliente', () => {
  it('should create an instance', () => {
    expect(new Cliente()).toBeTruthy();
  });
});

describe('Casos de prueba para comprobar si el teléfono fue insertado corrrectamente.', () => {
  let cliente: Cliente;

  beforeEach(() => {
    cliente = new Cliente();
  });
  
  /*
  Entradas:
      84780104, 
      85473777, 
      89852907,
      11111111, 
      99999999
  Se verifica en cada una de las iteraciones, si el elemento de la lista que acaba de ser insertado,
  es el que se obtiene, que sea igual.
  */
  it('Verfica si el telefono fue insertado correctamente.', () => {
    [
      84780104,
      85473777,
      89852907,
      11111111,
      99999999
    ].forEach((element) => {
      cliente.setTelefono(element);
      expect(cliente.getTelefono()).toEqual(element);
    });
  });
});