import { Venta } from './venta.model';
import { VentaDetalle } from '../venta-detalle-class/venta-detalle.model';
import { Producto } from '../producto-class/producto.model';

describe('Venta', () => {
  it('should create an instance', () => {
    expect(new Venta()).toBeTruthy();
  });
});

describe('Casos de prueba para calcular el subtotal de una venta.', () => {
  let venta: Venta;
  let arroz: Producto;
  let frijoles: Producto;
  let detalle1: VentaDetalle;
  let detalle2: VentaDetalle;

  beforeEach(() => {
    venta = new Venta();
    arroz = new Producto();
    frijoles = new Producto();
    detalle1 = new VentaDetalle();
    detalle2 = new VentaDetalle();

    arroz.setNombre("Arroz");
    arroz.setPrecioVenta(1300);

    frijoles.setNombre("Frijoles");
    frijoles.setPrecioVenta(900);

    detalle1.setProducto(arroz);
    detalle1.setCantidad(2);
    detalle1.setPrecio();

    detalle2.setProducto(frijoles);
    detalle2.setCantidad(1);
    detalle2.setPrecio();

  });

  /*
  Item 1: Arroz
    Precio: 1300
    Cantidad: 2
  Item 2: Frijoles
    Precio: 900
    Cantidad: 1
  Suma de los productos:
    (2*1300)+(1*900) = 3500
  Por lo tanto, subtotal esperado: 3500
  */
  it('Subtotal con n=2 productos.', () => {
    venta.setVentaDetalle(detalle1);
    venta.setVentaDetalle(detalle2);
    expect(venta.calcularSubTotal()).toBe(3500);
  });
  /*
La venta no presenta ningún producto.
Suma de los productos = 0
Por lo tanto, subtotal esperado: 0
*/
  it('Subtotal con 0 productos.', () => {
    expect(venta.calcularSubTotal()).toBe(0);
  });

});

describe('Casos de prueba para calcular el impuesto de una venta.', () => {
  let venta: Venta;
  let arroz: Producto;
  let frijoles: Producto;
  let detalle1: VentaDetalle;
  let detalle2: VentaDetalle;

  beforeEach(() => {
    venta = new Venta();
    arroz = new Producto();
    frijoles = new Producto();
    detalle1 = new VentaDetalle();
    detalle2 = new VentaDetalle();

    arroz.setNombre("Arroz");
    arroz.setPrecioVenta(1300);

    frijoles.setNombre("Frijoles");
    frijoles.setPrecioVenta(900);

    detalle1.setProducto(arroz);
    detalle1.setCantidad(2);
    detalle1.setPrecio();

    detalle2.setProducto(frijoles);
    detalle2.setCantidad(1);
    detalle2.setPrecio();

  });

  /*
  Item 1: Arroz
    Precio: 1300
    Cantidad: 2
    Porcentaje de IVA: 13%
  Item 2: Frijoles
    Precio: 900
    Cantidad: 1
    Porcentaje de IVA: 13%
  Cálculo del impuesto de ventas agregado de los productos:
    (2*1300*(0.13/100))+(1*900*(0.13/100)) = 4.55
  Por lo tanto, el impuesto esperado: 4.55
  */
  it('Impuesto con valor de impuesto agregado.', () => {
    detalle1.setPorcenIva(0.13);
    detalle2.setPorcenIva(0.13);
    venta.setVentaDetalle(detalle1);
    venta.setVentaDetalle(detalle2);
    expect(venta.calcularImpuesto()).toBe(4.55);
  });
  /*
  Item 1: Arroz
    Precio: 1300
    Cantidad: 2
    Porcentaje de IVA: 0%
  Item 2: Frijoles
    Precio: 900
    Cantidad: 1
    Porcentaje de IVA: 0%
  Cálculo del impuesto de ventas agregado de los productos:
    (2*1300*(0/100))+(1*900*(0/100)) = 0
  Por lo tanto, el impuesto esperado: 0
  */
  it('Impuesto sin valor de impuesto agregado.', () => {
    detalle1.setPorcenIva(0.0);
    detalle2.setPorcenIva(0.0);
    venta.setVentaDetalle(detalle1);
    venta.setVentaDetalle(detalle2);
    expect(venta.calcularImpuesto()).toBe(0);
  });

});

describe('Casos de prueba para calcular el descuento de una venta.', () => {
  let venta: Venta;
  let arroz: Producto;
  let frijoles: Producto;
  let detalle1: VentaDetalle;
  let detalle2: VentaDetalle;

  beforeEach(() => {
    venta = new Venta();
    arroz = new Producto();
    frijoles = new Producto();
    detalle1 = new VentaDetalle();
    detalle2 = new VentaDetalle();
	
    arroz.setNombre("Arroz");
    arroz.setPrecioVenta(1300);

    frijoles.setNombre("Frijoles");
    frijoles.setPrecioVenta(900);

    detalle1.setProducto(arroz);
    detalle1.setCantidad(2);
    detalle1.setPrecio();

    detalle2.setProducto(frijoles);
    detalle2.setCantidad(1);
    detalle2.setPrecio();

  });
  /*
  Item 1: Arroz
    Precio: 1300
    Cantidad: 2
    Porcentaje de descuento: 10%
  Item 2: Frijoles
    Precio: 900
    Cantidad: 1
    Porcentaje de descuento: 10%
  Cálculo del descuento de los productos:
    (2*1300*(0.10/100))+(1*900*(0.10/100)) = 3.5
  Por lo tanto, el impuesto esperado: 3.5
  */
  it('Descuento con valor de descuento agregado.', () => {
    detalle1.setPorcenDesc(0.1);
    detalle2.setPorcenDesc(0.1);
    venta.setVentaDetalle(detalle1);
    venta.setVentaDetalle(detalle2);
    expect(venta.calcularDescuento()).toBe(3.5);
  });
  /*
  Item 1: Arroz
    Precio: 1300
    Cantidad: 2
    Porcentaje de descuento: 0%
  Item 2: Frijoles
    Precio: 900
    Cantidad: 1
    Porcentaje de descuento: 0%
  Cálculo del descuento de los productos:
    (2*1300*(0/100))+(1*900*(0/100)) = 0
  Por lo tanto, el impuesto esperado: 0
  */
  it('Descuento sin valor de descuento agregado.', () => {
    detalle1.setPorcenDesc(0.0);
    detalle2.setPorcenDesc(0.0);
    venta.setVentaDetalle(detalle1);
    venta.setVentaDetalle(detalle2);
    expect(venta.calcularDescuento()).toBe(0);
  });

});